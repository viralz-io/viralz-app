import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { Plugins, StatusBarStyle } from '@capacitor/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private platform: Platform) { }

  async ngOnInit() {
    if (this.platform.is('capacitor')) {
      await Plugins.StatusBar.setStyle({ style: StatusBarStyle.Dark });
      await Plugins.SplashScreen.hide();
    }
  }
}
