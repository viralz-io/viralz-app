import { Component, ViewChild } from '@angular/core';
import { RestService } from '../services/rest.service';
import { AnimationController, IonContent } from '@ionic/angular';
import { Plugins } from '@capacitor/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  videoLink = '';
  showLoading = false;
  results = null;
  dlProgress = 0;
  currentMedia = 0;
  currentResolution = 0;

  @ViewChild('content') content: IonContent;

  constructor(private restService: RestService, private animationCtrl: AnimationController) { }

  async getPostMeta() {
    this.showLoading = true;
    if (this.videoLink) {
      try {
        const results = await this.restService.getMedia(this.videoLink);
        this.results = results;
        await this.fadeOut('input');
        await this.fadeIn('results');
        await this.content.scrollToBottom();
        // if (results.media.length === 1 && results.media[0].resolutions.length === 1) {
        //   await this.fadeOut('input');
        //   await this.fadeOut('progress-bar');
        //   this.download(results.media[0].resolutions.url);
        // } else if (results.media.length > 1) {
        //   await this.fadeOut('input');
        //   this.results = results;
        //   await this.fadeIn('results');
        // }
      } catch (e) {
        console.error(e);
      }
    } else {
      await Plugins.Modals.alert({
        title: 'Platform not supported',
        message: 'Sorry! This platform is currently not supported.'
      });
    }
    this.showLoading = false;


  }

  async startOver() {
    this.videoLink = '';
    await this.fadeOut('results');
    this.results = null;
    await this.fadeIn('input');
  }

  download() {
    const link = this.results.media[this.currentMedia].resolutions[this.currentResolution].url;
    this.restService.downloadFile('https://cors.js-frameworks.com/?' + link).subscribe(resp => {
      if (resp.state === 'IN_PROGRESS') {
        this.dlProgress = resp.progress / 100;
        // this.results[fileIndex].dlProgress = resp.progress;
      } else if (resp.state === 'DONE') {

      }
    })
  }

  getDirectLink() {
    return this.results ? this.results.media[this.currentMedia].resolutions[this.currentResolution].url : null;
  }

  forceDownload() {
    var anchor = document.createElement('a');
    anchor.href = this.results.media[this.currentMedia].resolutions[this.currentResolution].url;
    anchor.download = this.results.media[this.currentMedia].resolutions[this.currentResolution].url;
    document.body.appendChild(anchor);
    anchor.click();
  }

  changeMedia(e: any) {
    console.log('Event', e);
    console.log('currentMedia', this.currentMedia);
  }

  private fadeOut(element: string) {
    return this.animationCtrl.create()
      .addElement(document.getElementById(element))
      .duration(500)
      .fromTo('opacity', '1', '0')
      .beforeStyles({
        'display': 'inherit',
        'opacity': '1'
      })
      .afterStyles({
        'display': 'none'
      }).play();
  }

  private fadeIn(element: string) {
    return this.animationCtrl.create()
      .addElement(document.getElementById(element))
      .duration(300)
      .fromTo('opacity', '0', '1')
      .beforeStyles({
        'display': 'inherit',
        'opacity': '0'
      }).play();
  }

}
