import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpEventType, HttpResponse, HttpProgressEvent } from '@angular/common/http';
import { saveAs } from 'file-saver';
import { Observable } from 'rxjs';
import { scan } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private httpClient: HttpClient) { }

  getMedia(url: string): Promise<any> {
    return this.httpClient.get(`${environment.apiUrl}/?url=${url}`).toPromise();
  }

  downloadFile(url: string): Observable<Download> {
    const filename = 'vidoo-' + (url.split('/').pop().split('#')[0].split('?')[0] || Math.random().toString(36).substring(7) + '.mp4');
    console.log('FILENAME', filename);
    return this.httpClient.get(url, {
      reportProgress: true,
      observe: 'events',
      responseType: 'blob'
    }).pipe(this.download(blob => saveAs(blob, filename)));
  }

  private download(saver?: (b: Blob) => void): (source: Observable<HttpEvent<Blob>>) => Observable<Download> {
    return (source: Observable<HttpEvent<Blob>>) =>
      source.pipe( 
        scan((previous: Download, event: HttpEvent<Blob>): Download => {
            if (this.isHttpProgressEvent(event)) {
              return {
                progress: event.total
                  ? Math.round((100 * event.loaded) / event.total)
                  : previous.progress,
                state: 'IN_PROGRESS',
                content: null
              }
            }
            if (this.isHttpResponse(event)) {
              if (saver && event.body) {
                saver(event.body)
              }
              return {
                progress: 100,
                state: 'DONE',
                content: event.body
              }
            }
            return previous
          },
          {state: 'PENDING', progress: 0, content: null}
        )
      )
  }

  private isHttpResponse<T>(event: HttpEvent<T>): event is HttpResponse<T> {
    return event.type === HttpEventType.Response
  }
  
  private isHttpProgressEvent(event: HttpEvent<unknown>): event is HttpProgressEvent {
    return event.type === HttpEventType.DownloadProgress 
        || event.type === HttpEventType.UploadProgress
  }
}

export interface Download {
  state: 'PENDING' | 'IN_PROGRESS' | 'DONE'
  progress: number
  content: Blob | null
}